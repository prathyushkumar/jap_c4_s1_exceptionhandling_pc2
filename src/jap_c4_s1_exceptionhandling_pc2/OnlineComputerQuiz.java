package jap_c4_s1_exceptionhandling_pc2;

import java.util.Scanner;

public class OnlineComputerQuiz {
	Scanner sc = new Scanner(System.in);
	int n = sc.nextInt();
	SchoolNameAndScore arr[] = new SchoolNameAndScore[n];
	String name, score;
	void schoolsquizdata() {
		sc.nextLine();
		for (int i = 0; i < n; i++) {
			System.out.println("Enter school name " + (i + 1));
			name = sc.nextLine();
			System.out.println("Enter school score " + (i + 1));
			score = sc.nextLine();
			arr[i] = new SchoolNameAndScore(name, score);
			
		}
	}

	void displayHighestScoreAndAverageScore() {
		int highestScore = 0;
		float averageScore = 0;
		for (int i = 0; i < n; i++) {
			Integer val = Integer.parseInt(arr[i].getSchoolScore());
			averageScore += val;
			if (val > highestScore)
				highestScore = val;
		}
		String str = Integer.toString(highestScore);
		for (int i = 0; i < n; i++) {
			if (arr[i].getSchoolScore().equals(str))
				System.out.println("Highest Score in the quiz is " + arr[i].getSchoolName() + " School");
		}
		System.out.println("Average of School quiz Score is " + averageScore / arr.length);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter total No.of schools ");
		OnlineComputerQuiz ocq = new OnlineComputerQuiz();
		try {
			ocq.schoolsquizdata();
			ocq.displayHighestScoreAndAverageScore();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
