package jap_c4_s1_exceptionhandling_pc2;

public class SchoolNameAndScore {
	private String schoolName;
	private String schoolScore;
	public SchoolNameAndScore() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SchoolNameAndScore(String schoolName, String schoolScore) {
		super();
		this.schoolName = schoolName;
		this.schoolScore = schoolScore;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolScore() {
		return schoolScore;
	}
	public void setSchoolScore(String schoolScore) {
		this.schoolScore = schoolScore;
	}
	@Override
	public String toString() {
		return "SchoolNameAndScore [schoolName=" + schoolName + ", schoolScore=" + schoolScore + "]";
	}
	
	
}
